import { Low } from 'lowdb';
import { JSONFile } from 'lowdb/node'
import merge from 'lodash.merge';
import path from 'path';
import { v4 as uuidv4 } from 'uuid';

export default class SimpleDB {
    db;
    file;
    options;

    constructor(options = {}) {
        this.options = options;
        this.file = path.resolve(this.options?.filePath || './db.json');
        this.db = new Low(new JSONFile(this.file));
    }

    /**
     * 
     * @returns list of objects
     */
    async get() {
        await this.db.read();

        return this.db?.data?.records || [];
    }

    /**
     * 
     * @param {string} uuid 
     * @returns object
     */
    async find(uuid) {
        await this.db.read();

        const records = await this.get();

        return records.find(item => item.uuid == uuid);
    }

    /**
     * 
     * @param {string} uuid
     * @returns boolean
     */
    async exists(uuid) {
        return !!SimpleDB.find(uuid);
    }

    /**
     * 
     * @param {object} record 
     */
    async create(record) {
        if (typeof record !== 'object') {
            throw new Error("Record must be an object");
        }

        if (record.uuid) {
            throw new Error("Record uuid must be null or undefined");
        }

        const records = await this.get();

        records.push({
            ...record,
            uuid: uuidv4(),
            created: new Date(),
            updated: new Date()
        });

        this.db.data = {
            ...this.db.data,
            records
        }

        await this.db.write();
    }

    /**
     * 
     * @param {object} record updates record by given uuid inside the object
     */
    async update(uuid, record) {
        if (typeof record !== 'object') {
            throw new Error("Record must be an object");
        }

        if (!uuid) {
            throw new Error("Record uuid not defined");
        }

        if (record.uuid) {
            throw new Error("Record uuid not allowed in payload");
        }

        const records = await this.get();

        let foundRecord = records.find(a => a.uuid == uuid);

        if (!foundRecord) {
            throw new Error("Record not found");
        }

        this.db.data = {
            ...this.db.data,
            records: records.map(item => {
                if (item.uuid == uuid) {
                    return merge(foundRecord, { ...record, updated: new Date() });
                } else {
                    return item;
                }
            })
        }

        await this.db.write();
    }

    /**
     * 
     * @param {string} uuid 
     */
    async delete(uuid) {
        if (!uuid) {
            throw new Error("Record uuid not defined");
        }

        await this.db.read();

        const records = await this.get();

        let foundRecord = records.find(a => a.uuid == uuid);

        if (!foundRecord) {
            throw new Error("Record not found");
        }

        this.db.data = {
            ...this.db.data,
            records: records.filter(item => item.uuid != uuid)
        }

        await this.db.write();
    }
}